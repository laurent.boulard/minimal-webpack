# 🚀 Simple webpack project

This is a simple project that create static assets for a HTML only site.

Based on Gitea project
[ webpack configuration](https://github.com/go-gitea/gitea/blob/v1.13.0-rc2/webpack.config.js).

## Project

Static HTML page is defined in `public/index.html`.
Used resources are defined in `web-src\index.js` and `web-src\extra.js`.
Then, at build time, files output of `webpack` command is pushed to subfolders
of `public` folder.

Icons used for this simple project:

- Single SVG images files from <https://primer.style/octicons/>
- Icons with CSS and a web font with <https://fontawesome.com/v4.7.0/icons/>
- SVG sprites from <https://www.npmjs.com/package/svg-icon>

## Development

First, run `yarn` alone to install required packages into `node_modules`.

Run "`yarn build`" for production, "`yarn build-dev`" for development.
Continuous development using "`yarn watch`". Edit file and `public` folder
will be updated continuously. Command are shortcuts defined in
`webpack.config.js`.

Due to security issue, SVG sprites do not work from `file://` scheme. Hence,
opening file `index.html` in browser show missing SVG images.

Serves site using "`py -m http.server -d public`" when python is installed on
host. Else, use `webpack` server, run "`npx webpack serve --watch`" to start
server and monitor files changes to trigger rebuild. Add "`--mode development`"
for a development build. To ease working on project, shortcuts are defined in
`package.json`: "`yarn start`" and "`yarn start-dev`". More configuration
options are defined in `webpack.config.js` at `devServer` property.

To open automatically browser for debug, add "`--open <browser path>`".
Example on Windows with [Vivaldi Browser](https://vivaldi.com):

```sh
# Dos batch
yarn start --open "%LOCALAPPDATA%\Vivaldi\Application\vivaldi.exe"
# Powershell
yarn start --open "${ENV:LOCALAPPDATA}\Vivaldi\Application\vivaldi.exe"
```

### Docker

`Dockerfile` builds static content with Node image for docker and use Nginx to
publish static content of `public/` folder.

```sh
docker build -t minimal-webpack .
docker run --rm --name minimal-webpack-0 -p 8000:80 minimal-webpack
```

Open browser on <http://localhost:8000> to access to static web site.

Stop container with `<CTRL-C>` or with command `docker stop minimal-webpack-0`
in another shell.
