FROM node:14-alpine AS build

WORKDIR /app

COPY ["package.json", "yarn.lock", "./"]
RUN yarn install

COPY "webpack.config.js" "./"
COPY "./public" "./public"
COPY "./web-src" "./web-src"
RUN yarn build

FROM nginx:alpine
COPY --from=build /app/public/ /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
