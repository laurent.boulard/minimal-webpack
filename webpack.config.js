const webpack = require("webpack");
const { statSync } = require("fs");
const { resolve } = require("path");

const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const isProduction = process.env.NODE_ENV !== "development";

const filterCssImport = (url, ...args) => {
  const cssFile = args[1] || args[0]; // resourcePath is 2nd argument for url and 3rd for import
  const importedFile = url.replace(/[?#].+/, "").toLowerCase();

  // Keep only woff2
  if (cssFile.includes("font-awesome")) {
    if (/(eot|ttf|otf|woff|svg)$/.test(importedFile)) return false;
  }

  return true;
};

module.exports = {
  mode: isProduction ? "production" : "development",

  entry: {
    index: "./web-src/index.js",
    extra: "./web-src/extra.js",
  },

  devtool: "inline-source-map",

  // https://webpack.js.org/configuration/dev-server/
  devServer: {
    contentBase: resolve(__dirname, "public"),
    port: 8000,
  },

  output: {
    path: resolve(__dirname, "public"),
    filename: "js/[name].js",
    chunkFilename: "js/[name].js",
  },

  plugins: [
    new webpack.ProgressPlugin(),

    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "css/[name].css",
    }),

    new webpack.SourceMapDevToolPlugin({
      filename: "[file].map",
      include: ["js/index.js", "js/extra.js", "css/index.css"],
    }),
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              sourceMaps: true,
              cacheDirectory: true,
              cacheCompression: false,
              cacheIdentifier: [
                resolve(__dirname, "package.json"),
                resolve(__dirname, "yarn.lock"),
                resolve(__dirname, "webpack.config.js"),
              ]
                .map((path) => statSync(path).mtime.getTime())
                .join(":"),
              presets: [
                [
                  "@babel/preset-env",
                  {
                    useBuiltIns: "usage",
                    corejs: 3,
                  },
                ],
              ],
              plugins: [
                [
                  "@babel/plugin-transform-runtime",
                  {
                    regenerator: true,
                  },
                ],
              ],
              generatorOpts: {
                compact: false,
              },
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
              url: filterCssImport,
              import: filterCssImport,
            },
          },
        ],
      },

      {
        test: /\.svg$/i,
        // include: resolve(__dirname, 'public/img/svg'),
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "svg/",
              publicPath: (url) => `../svg/${url}`, // required to remove css/ path segment
            },
          },
        ],
      },
      {
        test: /\.(ttf|woff2?)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/",
              publicPath: (url) => `../fonts/${url}`, // required to remove css/ path segment
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|jpeg)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "img/",
              publicPath: (url) => `../img/${url}`, // required to remove css/ path segment
            },
          },
        ],
      },
    ],
  },

  optimization: {
    minimizer: [
      new TerserPlugin(),
      new CssMinimizerPlugin({
        sourceMap: true,
        minimizerOptions: {
          preset: [
            "default",
            {
              discardComments: {
                removeAll: true,
              },
            },
          ],
        },
      }),
    ],
  },

  performance: {
    hints: false,
    maxEntrypointSize: Infinity,
    maxAssetSize: Infinity,
  },

  resolve: {
    symlinks: false,
    alias: {
      "~": resolve(__dirname, "node_modules"),
    },
  },

  watchOptions: {
    ignored: ["node_modules/**"],
  },
};
