//
import "~/font-awesome/css/font-awesome.css";

// https://primer.style/octicons/
import "~/@primer/octicons/build/svg/alert-24.svg";
import "~/@primer/octicons/build/svg/bell-24.svg";
import "~/@primer/octicons/build/svg/arrow-both-24.svg";
import "~/@primer/octicons/build/svg/arrow-down-24.svg";
import "~/@primer/octicons/build/svg/arrow-down-left-24.svg";
import "~/@primer/octicons/build/svg/arrow-down-right-24.svg";
import "~/@primer/octicons/build/svg/arrow-left-24.svg";
import "~/@primer/octicons/build/svg/arrow-right-24.svg";
import "~/@primer/octicons/build/svg/arrow-switch-24.svg";
import "~/@primer/octicons/build/svg/arrow-up-24.svg";
import "~/@primer/octicons/build/svg/arrow-up-left-24.svg";
import "~/@primer/octicons/build/svg/arrow-up-right-24.svg";
import "~/@primer/octicons/build/svg/container-24.svg";
import "~/@primer/octicons/build/svg/cpu-24.svg";
import "~/@primer/octicons/build/svg/check-circle-fill-24.svg";
import "~/@primer/octicons/build/svg/x-circle-fill-24.svg";
import "~/@primer/octicons/build/svg/link-24.svg";

import "./css/index.css";
import "./css/main.css";
